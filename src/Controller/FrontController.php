<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class FrontController extends AbstractController
{
    /**
     * @Route("/", name="front_home")
     */
    public function index()
    {
        $em = $this->getDoctrine()->getManager();
        return $this->render('front/index.html.twig', [
            'controller_name' => 'FrontController',
            'departments' => $em->getRepository('App:Department')->findAll()
        ]);
    }
    /**
     * @Route("/list-page", name="front_list_page")
     */
    public function listPage()
    {
        $em = $this->getDoctrine()->getManager();
        return $this->render('front/list.html.twig', [
            'controller_name' => 'FrontController'
        ]);
    }

    /**
     * @Route("/empty-page", name="front_empty_page")
     */
    public function emptyPage()
    {
        $em = $this->getDoctrine()->getManager();
        return $this->render('front/empty.html.twig', [
            'controller_name' => 'FrontController'
        ]);
    }
}
